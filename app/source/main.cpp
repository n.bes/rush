#include "founder.h"
#include "splitter.h"
#include <fstream>
#include <iostream>
#include <string>
#include <memory>
#include <array>
#include <iomanip>

int main(int argc, char const *argv[]){
	if (argc != 4){
		std::cout << argv[0] << " <input file> <pattern file> <threshold>" << std::endl;
		return 1;
	}

	try{
		std::ifstream inputFile(argv[1],  std::ios::binary);
		std::ifstream patternFile(argv[2],  std::ios::binary);

		if (!inputFile) {
			throw std::runtime_error(argv[1]);
		}
		if (!patternFile) {
			throw std::runtime_error(argv[2]);
		}
		std::vector<std::uint8_t> patternFileData(
			(std::istreambuf_iterator<char>(patternFile)),
			std::istreambuf_iterator<char>()
		);


		auto threshold = std::stoul(argv[3]);
		Splitter splitter(threshold);
		auto patterns = splitter.split(patternFileData);

		std::vector<std::shared_ptr<Founder>> founders;
		for (const auto &pattern: patterns){
			auto founder = std::make_shared<Founder>(pattern);
			founder->onFound.connect([founder](int position){
				auto pattern = founder->getPattern();
				auto str = std::string(pattern.begin(), pattern.end());
				std::cout << "pos: " << std::setw(6) << std::left << position
					<< "size: " << std::setw(6) << std::left << str.size()
					<< "pattern: " << str << std::endl;
			});
			founders.push_back(founder);
		}

		const int readSize = 2048;
		std::array<std::uint8_t, readSize> array;
		bool read = true;
		while(read){
			int realRead = readSize;

			inputFile.read((char*)&array[0], readSize);

			if (!inputFile){
				realRead = inputFile.gcount();
				read = false;
			}

			for(int i=0; i<realRead; ++i){
				for (const auto &founder: founders){
					founder->put(array[i]);
				}
			}
		}
	}
	catch(const std::exception &e){
		std::cerr << "Exception: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
