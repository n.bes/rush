#include "splitter.h"


Splitter::Splitter(const unsigned int &threshold): min(threshold){
	if (threshold < 1){
		throw std::invalid_argument("threshold must be > 0");
	}
}

std::vector <std::vector <std::uint8_t>> Splitter::split(const std::vector <std::uint8_t> &data){
	std::vector <std::vector <std::uint8_t>> result;

	for (auto size = min; size <= data.size(); ++size){
		for(auto shift = 0u; shift <= data.size() - size; ++shift){

			std::vector <std::uint8_t> sub;
			for (auto i = shift; i < size + shift; ++i){
				sub.push_back(data[i]);
			}

			result.push_back(sub);
		}
	}
	return result;
}
