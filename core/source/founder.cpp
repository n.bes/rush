#include "founder.h"
Founder::Founder(const std::vector <std::uint8_t> &pattern) : pattern(pattern){
	if (pattern.empty()){
		throw std::invalid_argument("pattern can't be empty");
	}
}

void Founder::put(const std::uint8_t &data){
	total++;

	if (pattern[counter] != data){
		counter = 0;
	}
	else{
		counter++;

		if (pattern.size() == counter){
			counter = 0;
			onFound.emit(total-pattern.size());
		}
	}
};

std::vector <std::uint8_t> Founder::getPattern() const{
	return pattern;
}
