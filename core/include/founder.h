#pragma once
#include <simpleSignal.h>
#include <vector>
#include <exception>

class Founder{
	int counter = 0;
	int total = 0;

	std::vector <std::uint8_t> pattern;
public:
	Founder(const std::vector <std::uint8_t> &pattern);
	std::vector<std::uint8_t> getPattern() const;
	void put(const std::uint8_t &data);
	SimpleSignal <const int&> onFound;
};
