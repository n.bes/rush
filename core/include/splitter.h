#pragma once
#include <vector>
#include <iterator>

class Splitter{
	unsigned int min;
public:
	Splitter(const unsigned int &threshold = 1);
	std::vector <std::vector <std::uint8_t>> split(const std::vector <std::uint8_t> &data);
};
