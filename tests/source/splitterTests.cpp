#include "splitter.h"
#include <catch.hpp>
#include <vector>

TEST_CASE("ctor [splitter]"){
	SECTION("default"){
		Splitter s;
	}
	SECTION("invalid_argument"){
		int threshold = 0;
		REQUIRE_THROWS_AS(std::make_shared<Splitter>(threshold), std::invalid_argument);
	}
}

TEST_CASE("split threshold=1 [splitter]"){
	int threshold = 1;
	Splitter s(threshold);

	SECTION("size 1"){
		std::vector <std::uint8_t> data = {0x00};
		auto fragments = s.split(data);
		REQUIRE(fragments.size() == 1);
		REQUIRE_FALSE(fragments[0].empty());
		REQUIRE(fragments[0][0] == 0x00);
	};

	SECTION("size 2"){
		std::vector <std::uint8_t> data = {0x00, 0x01};
		auto fragments = s.split(data);
		REQUIRE(fragments.size() == 3);
	};

	SECTION("size 3"){
		std::vector <std::uint8_t> data = {0x00, 0x01, 0x02};
		auto fragments = s.split(data);
		REQUIRE(fragments.size() == 6);
	};

	SECTION("size 4"){
		std::vector <std::uint8_t> data = {0x00, 0x01, 0x02, 0x03};
		auto fragments = s.split(data);
		REQUIRE(fragments.size() == 10);
	};
}

TEST_CASE("split threshold=2 [splitter]"){
	int threshold = 2;
	Splitter s(threshold);

	SECTION("size 1"){
		std::vector <std::uint8_t> data = {0x00};
		auto fragments = s.split(data);
		REQUIRE(fragments.size() == 0);
	};

	SECTION("size 2"){
		std::vector <std::uint8_t> data = {0x00, 0x01};
		auto fragments = s.split(data);
		REQUIRE(fragments.size() == 1);
	};

	SECTION("size 3"){
		std::vector <std::uint8_t> data = {0x00, 0x01, 0x02};
		auto fragments = s.split(data);
		REQUIRE(fragments.size() == 3);
	};

	SECTION("size 4"){
		std::vector <std::uint8_t> data = {0x00, 0x01, 0x02, 0x03};
		auto fragments = s.split(data);
		REQUIRE(fragments.size() == 6);
	};

	SECTION("text"){
		std::string raw("abc");
		auto data = std::vector <std::uint8_t>(raw.begin(), raw.end());

		auto fragments = s.split(data);
		REQUIRE(fragments.size() == 3);

		REQUIRE("ab" == std::string(fragments[0].begin(), fragments[0].end()));
		REQUIRE("bc" == std::string(fragments[1].begin(), fragments[1].end()));
		REQUIRE("abc" == std::string(fragments[2].begin(), fragments[2].end()));
	};

	SECTION("size 1000"){
		auto data = std::vector <std::uint8_t>(1000);
		auto fragments = s.split(data);
	};
};
TEST_CASE("split threshold=100 [splitter]"){
	int threshold = 1000;
	Splitter s(threshold);
	auto data = std::vector <std::uint8_t>(2000);
	auto fragments = s.split(data);
	REQUIRE(fragments.size() == 501501);
}


TEST_CASE("split threshold=3 [splitter]"){
	int threshold = 3;
	Splitter s(threshold);

	SECTION("size 1"){
		std::vector <std::uint8_t> data = {0x00};
		auto fragments = s.split(data);
		REQUIRE(fragments.size() == 0);
	};

	SECTION("size 2"){
		std::vector <std::uint8_t> data = {0x00, 0x01};
		auto fragments = s.split(data);
		REQUIRE(fragments.size() == 0);
	};

	SECTION("size 3"){
		std::vector <std::uint8_t> data = {0x00, 0x01, 0x02};
		auto fragments = s.split(data);
		REQUIRE(fragments.size() == 1);
	};

	SECTION("size 4"){
		std::vector <std::uint8_t> data = {0x00, 0x01, 0x02, 0x03};
		auto fragments = s.split(data);
		REQUIRE(fragments.size() == 3);
	};
};
