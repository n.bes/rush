#include "founder.h"
#include "splitter.h"

#include <catch.hpp>

#include <algorithm>
#include <array>
#include <fstream>
#include <iterator>
#include <memory>
#include <string>
#include <vector>

TEST_CASE("simple [intergration]"){
	std::ifstream inputFile(RESOURCES_DIR "input.bin",  std::ios::binary);
	std::ifstream patternFile(RESOURCES_DIR "pattern.bin",  std::ios::binary);
	REQUIRE(inputFile);
	REQUIRE(patternFile);

	std::vector<std::uint8_t> patternFileData(
		(std::istreambuf_iterator<char>(patternFile)),
		std::istreambuf_iterator<char>()
	);
	REQUIRE(patternFileData.size() == 16);


	int threshold = 15;
	Splitter splitter(threshold);

	auto patterns = splitter.split(patternFileData);
	REQUIRE(patterns.size() == 3);


	std::vector<int> positions;
	std::vector<std::shared_ptr<Founder>> founders;
	for (const auto &pattern: patterns){
		auto founder = std::make_shared<Founder>(pattern);
		founder->onFound.connect([&](int position){
			positions.push_back(position);
		});
		founders.push_back(founder);
	}

	const int readSize = 2;
	std::array<std::uint8_t, readSize> array;
	bool read = true;
	while(read){
		int realRead = readSize;

		inputFile.read((char*)&array[0], readSize);

		if (!inputFile){
			realRead = inputFile.gcount();
			read = false;
		}

		for(int i=0; i<realRead; ++i){
			for (const auto &founder: founders){
				founder->put(array[i]);
			}
		}
	}

	std::sort(positions.begin(), positions.end());
	REQUIRE(positions.size() == 4);

	REQUIRE(positions[0] == 16);
	REQUIRE(positions[1] == 16);
	REQUIRE(positions[2] == 17);
	REQUIRE(positions[3] == 128);
}
