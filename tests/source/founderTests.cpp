#include "founder.h"
#include <catch.hpp>
#include <vector>
#include <string>

TEST_CASE("ctor [founder]"){
	SECTION("default"){
		Founder f({0x00});
	}

	SECTION("invalid_argument"){
		std::vector <std::uint8_t> rrr;
		REQUIRE_THROWS_AS(std::make_shared<Founder>(rrr), std::invalid_argument);
	}
}


TEST_CASE("put [founder]"){
	SECTION("simple"){
		Founder f({0x00});
		bool found = false;

		f.onFound.connect([&](const int &position){
			found = true;
		});

		REQUIRE_FALSE(found);
		f.put(0x00);
		REQUIRE(found);
	}

	SECTION("pattern size = 1"){
		Founder f({0x00});
		std::vector <int> positions;
		f.onFound.connect([&](const int &position){
			positions.push_back(position);
		});

		f.put(0x01);
		f.put(0x00);
		f.put(0x01);
		f.put(0x00);

		REQUIRE(positions.size() == 2);
		REQUIRE(positions[0] == 1);
		REQUIRE(positions[1] == 3);
	}

	SECTION("pattern size = 2"){
		Founder f({0x00, 0x01});
		std::vector <int> positions;
		f.onFound.connect([&](const int &position){
			positions.push_back(position);
		});

		f.put(0x00);
		f.put(0x01);
		f.put(0x00);
		f.put(0x02);
		f.put(0x00);
		f.put(0x01);
		f.put(0x00);
		f.put(0x01);

		REQUIRE(positions.size() == 3);
		REQUIRE(positions[0] == 0);
		REQUIRE(positions[1] == 4);
		REQUIRE(positions[2] == 6);
	}

	SECTION("pattern size = 3"){
		Founder f({0x00, 0x01, 0x03});
		std::vector <int> positions;
		f.onFound.connect([&](const int &position){
			positions.push_back(position);
		});

		f.put(0x00);
		f.put(0x01);
		f.put(0x02);
		f.put(0x00);
		f.put(0x01);
		f.put(0x03);
		f.put(0x00);
		f.put(0x01);

		REQUIRE(positions.size() == 1);
		REQUIRE(positions[0] == 3);
	}

	SECTION("text"){
		std::string data = u8"abc";
		auto pattern = std::vector <std::uint8_t>(data.begin(), data.end());
		Founder f(pattern);

		std::vector <int> positions;
		f.onFound.connect([&](const int &position){
			positions.push_back(position);
		});

		std::string dataStream = u8"ZabcZZabZZdaZZabc";
		for (const auto &c: dataStream){
			f.put(c);
		}

		REQUIRE(positions.size() == 2);
		REQUIRE(positions[0] == 1);
		REQUIRE(positions[1] == 14);
	}
}
