# build
``` sh
git clone --recursive https://gitlab.com/nik_b/rush
cd rush
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
# make or msbuild or other
```

# example
run
```
PS C:\Users\rg\rush\bin\Release> .\rushApp.exe ..\..\tests\data\text_input.bin ..\..\tests\data\text_pattern.bin 3
```
output
```
pos: 9     size: 3     pattern: abc
pos: 27    size: 3     pattern: bcd
pos: 28    size: 3     pattern: cd1
pos: 27    size: 4     pattern: bcd1
pos: 29    size: 3     pattern: d12
pos: 28    size: 4     pattern: cd12
pos: 27    size: 5     pattern: bcd12
pos: 38    size: 3     pattern: cd1
pos: 39    size: 3     pattern: d12
pos: 38    size: 4     pattern: cd12
pos: 40    size: 3     pattern: 123
pos: 39    size: 4     pattern: d123
pos: 38    size: 5     pattern: cd123
```
